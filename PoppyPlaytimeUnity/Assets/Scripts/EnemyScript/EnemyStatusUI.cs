﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class EnemyStatusUI : EnemyScript
{

    [Header("Slider Timer References")]
    public GameObject sliderTimerObj;
    
    public Slider sliderTimer;
    public GameObject clockImg;
    public TextMeshProUGUI timeText;

    [Header("Slider Range References")]
    public GameObject sliderRangeOjb;
    public Slider sliderRange;
    public TextMeshProUGUI rangeText;
    public GameObject RunImg;
    private float rangeMove;
    [Header("Timer References")]
    public float timeStand;
    public float timeWander;
    private float startTime, currentTime;
    private int seconds;
    [Header("Notify Status References")]
    public GameObject notifyStatusPanel;
    public TextMeshProUGUI  notifyStatusText;
    public GameObject timeTextOjb;
    public GameObject runAnimOjb;
    public TextMeshProUGUI runAnimText;
    internal bool changeStatus;
    private bool isShowFollow = true, isShowFollowAnim = true, isShowTimeText = true;
    private Animator anim_NotifyStatus;
    // Start is called before the first frame update
    private void Start()
    {
        anim_NotifyStatus = notifyStatusPanel.GetComponent<Animator>();
        StartUIWithStandState();
        sliderRange.maxValue = Get<EnemyMovement>().rangeMoveToPlayer;
    }

    // Update is called once per frame
    void Update()
    {
        if(changeStatus)
        {
            NextStateOfEnemyUI();
            FormatTime();
        }
        changeStatus = false;
        currentTime -= 1f * Time.deltaTime;
        UpdateStateUI();
    }
    private void UpdateStateUI()
    {
         switch (Get<EnemyMovement>().currentState)
        {
            case EnemyMovement.EnemyState.Stand:
                UpdateTimeText();
                UpdateTimeSlider();
                break;
            case EnemyMovement.EnemyState.Move:
                UpdateRangeSlider();
                break;
            case EnemyMovement.EnemyState.Wander:
                UpdateTimeText();
                UpdateTimeSlider();
                break;    
            default:
                break;
        }   
    }
    void UpdateTimeSlider()
    {
        DisableNotifyStatusFollow();
        sliderTimer.value = currentTime;  
        if(sliderTimer.value == 0 && sliderTimerObj.activeSelf == true)   
            changeStatus = true;
        SoundManager.Instance.PlayClockSound();
    }
    void UpdateTimeText()
    {
        clockImg.SetActive(true);
        if(isShowTimeText)
        {
            timeTextOjb.SetActive(true);
            seconds = ((int)currentTime % 60);
            timeText.text = seconds.ToString("00");

        }
        else
        {
            timeTextOjb.SetActive(false);
        }
            
    }
    void UpdateRangeSlider()
    {
        timeTextOjb.SetActive(false);
        clockImg.SetActive(false);
        if(isShowTimeText)
        {
            float rangeToShow = Get<EnemyMovement>().rangeMoveToPlayer;
            rangeMove = Get<EnemyMovement>().distanceToPlayer;    
            if(rangeMove < rangeToShow)
            {
                timeTextOjb.SetActive(false);
                isShowFollow = true;   
                ShowNotifyStatusFollow();
            }
            UpdateColorRangeSlider();
            sliderRange.value = rangeMove;  
            rangeText.text = rangeMove.ToString("00");         
        }
    }
    private void UpdateColorRangeSlider()
    {
        float diviveRangeMoveToPlayer = Get<EnemyMovement>().rangeMoveToPlayer / 3;
        if(rangeMove > 0 && rangeMove < diviveRangeMoveToPlayer)
        {
            ColorRangeSlider(Color.red);
            runAnimText.text = "RUN FASTER";
        }
        if(rangeMove > diviveRangeMoveToPlayer && rangeMove < diviveRangeMoveToPlayer *2)
        {
            ColorRangeSlider(Color.yellow);
            runAnimText.text = "RUN";
        }
        if(rangeMove > diviveRangeMoveToPlayer * 2)
            ColorRangeSlider(Color.green);
    }
    private void ColorRangeSlider(Color color)
    {
        rangeText.faceColor = color;
        RunImg.GetComponent<Image>().color = color;
        sliderRangeOjb.GetComponentsInChildren<Image>()[0].color = new Color(color.r,color.g,color.b,0.3f);
        sliderRangeOjb.GetComponentsInChildren<Image>()[1].color = color;
    }

    public void UpdateNewRange() {
        sliderRange.maxValue = Get<EnemyMovement>().rangeMoveToPlayer += 5f;
        sliderRange.maxValue = Get<EnemyMovement>().rangeMoveToPlayer;
    }
    public void UpdateNewTimeWander() {
        timeWander -= 2f;
        if(timeWander <= 0)
            timeWander = 1f;
    }
    private void ShowNotifyStatusFollow()
    {
        if(isShowFollow && isShowFollowAnim)
        {
            notifyStatusText.faceColor = Color.red;
            notifyStatusText.text = "The Monter is following you";
            anim_NotifyStatus.SetTrigger("ShowNotifyStatus");
            SoundManager.Instance.PlayNotificationSound();
            StartCoroutine(ShowRunAnim(1.8f));
        }
        isShowFollow = false;
        isShowFollowAnim = false;
    }
    private void DisableNotifyStatusFollow()
    { 
        sliderRangeOjb.SetActive(false);
        runAnimOjb.SetActive(false);
    }
    private IEnumerator ShowRunAnim(float time)
    { 
        yield return new WaitForSeconds(time);
        sliderRangeOjb.SetActive(true);
        runAnimOjb.SetActive(true);
        runAnimText.faceColor = Color.red;
        runAnimOjb.GetComponent<Animator>().Play("RunAnim");
    }
    private void FormatTime()
    {
        sliderTimer.maxValue = startTime;
        sliderTimer.value = startTime;
        currentTime = startTime;
    }
    public void StartUIWithStandState()
    {
        startTime = timeStand;
        SetUIStatusColor(Color.green);
        notifyStatusText.text = "The Monter is standing";
        anim_NotifyStatus.SetTrigger("ShowNotifyStatus");
        SoundManager.Instance.PlayNotificationSound();
        timeText.faceColor = Color.green;
        clockImg.GetComponent<Image>().color = Color.green;
        FormatTime();
    }
    private void StartUIWithWanderState()
    {
        isShowFollow = true;
        isShowFollowAnim = true;
        startTime = timeWander;
        SetUIStatusColor(Color.yellow);
        SoundManager.Instance.PlayNotificationSound();
        notifyStatusText.text = "The Monter is wandering";
        anim_NotifyStatus.SetTrigger("ShowNotifyStatus");
    }
    private void StartUIWithKillState()
    {
        isShowTimeText = false;
        isShowFollow = false;
        isShowFollowAnim = false;
        DisableNotifyStatusFollow();
        timeTextOjb.SetActive(false);
    }
    public void DisableKillStateUI()
    {
        isShowTimeText = true;
        isShowFollow = true;
        isShowFollowAnim = true;
    }
    private void NextStateOfEnemyUI()
    {
         switch (Get<EnemyMovement>().currentState)
        {
            case EnemyMovement.EnemyState.Move:
                SoundManager.Instance.StopClockSound();
                SetUIStatusColor(Color.red);
                break;
            case EnemyMovement.EnemyState.Wander:
                StartUIWithWanderState();
                break;
            case EnemyMovement.EnemyState.KillPlayer:
                StartUIWithKillState();
                break;
            default:
                break;
        }
    }

    public void SetUIStatusColor(Color color)
    {
        if(Get<EnemyMovement>().currentState == EnemyMovement.EnemyState.Move)
            sliderTimerObj.SetActive(false);
        else
            sliderTimerObj.SetActive(true);
  
        timeText.faceColor = color;
        clockImg.GetComponent<Image>().color = color;
        notifyStatusText.faceColor = color;
        sliderTimerObj.GetComponentsInChildren<Image>()[1].color = color;
        Color backgroundColor = new Color(color.r,color.g,color.b,0.3f);
        sliderTimerObj.GetComponentsInChildren<Image>()[0].color = backgroundColor;
    }
    
}
