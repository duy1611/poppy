using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    private Animator anim ;
    private void Awake() {
        anim = GetComponentInChildren<Animator>();
    }
    public void WalkAnim()
    {
        // Change animation to walk
        anim.Play("walk");
    }

    public void StandAnim()
    {
        // Change animation to stand
        anim.Play("Idle");
    }
    public void ActackAnim()
    {   
        SoundManager.Instance.PlaySoundEnemyActack();
        anim.Play("attack");
    }

}
