using System.Collections;
using System;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : EnemyScript
{
    public enum EnemyState
    {
        Stand,
        Move,
        Wander,
        KillPlayer
    }
    public EnemyState currentState;
    [Header("Timer References")]
    [SerializeField] private float timeRequireToKill = 5f;
    private float currentKillTime = 0f;
    [Header("Range References")]
    //[SerializeField] private float rangeChase = 3f;
    [SerializeField] private float rangeKill = 1f;
    [SerializeField] public float rangeMoveToPlayer = 50f;
    internal float distanceToPlayer;
    private float rangeToPlayer;
    [Header("Player References")]
    public GameObject player; 
    public GameObject playerLookAtTarget; 
    [Header("WanderPoint Move")]
    public Transform RandomPointMove; 
    public float radius;
    private NavMeshAgent agent;
    private Vector3 destination;
    public float speedMove;
    private float speedMoveOrigin;
    private bool isRevive;
    internal bool isMovedToPlayer = false;
    private Vector3 defaultPosition;
    
    private void Start() {
        agent = GetComponent<NavMeshAgent>();
        speedMoveOrigin = GetComponent<NavMeshAgent>().speed;
        speedMove = speedMoveOrigin;
        defaultPosition = transform.position;
        OnStartStand();
    }

    private void Update()
    {
        if(Get<EnemyStatusUI>().changeStatus)
        { 
            if(isRevive)
                SwitchState(EnemyState.Wander);
            else
                NextState();
            
        }
        UpdateState();
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case EnemyState.Move:
                OnStartMove();
                break;
            case EnemyState.Wander:
                OnStartWander();
                break;
            default:
                break;
        }
    }

    public void SwitchState(EnemyState state)
    {
        currentState = state;
        switch (currentState)
        {
             case EnemyState.KillPlayer:
                OnStartKillPlayer();
                break;
            default:
                break;
        }
           
    }
    private void NextState()
    {
        EnemyState next = currentState + 1;
        next = next > EnemyState.Wander ? EnemyState.Move: next;
        SwitchState(next);       
    }
    private void OnStartStand()
    {
        Get<EnemyAnimation>().StandAnim();
        agent.isStopped = true;
        FaceToFacePlayer();
    }
    private void OnStartMove()
    {
        GetComponent<NavMeshAgent>().speed = speedMove;
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);  
        // When the monster move to player
        if(distanceToPlayer > rangeKill)
        {
            rangeToPlayer =  distanceToPlayer;
            if(distanceToPlayer < rangeMoveToPlayer)
            {
                isMovedToPlayer = true;
            }
            
            agent.isStopped = false;
            FaceToFacePlayer();
            agent.SetDestination(player.transform.position);
            Get<EnemyAnimation>().WalkAnim();
        }
        if(isMovedToPlayer && rangeToPlayer > rangeMoveToPlayer)
        {
            Get<EnemyStatusUI>().changeStatus = true; 
            NextState();
        }
        SoundManager.Instance.PlayEnemyFootStepSound();
        CloseToPlayer();
        isRevive = false;
    }
    
    private void OnStartWander()
    {
        // When the monster doesn't move to player
        GetComponent<NavMeshAgent>().speed = speedMove/3;
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        RandomPointMove.position = new Vector3(transform.position.x + 5,transform.position.y, transform.position.z+ 5);
        agent.isStopped = false;
        agent.SetDestination(GetPointRandomly.instance.GetRandomPoint());
        Get<EnemyAnimation>().WalkAnim();
        isMovedToPlayer = false;
        SoundManager.Instance.PlayEnemyWalkSound();
        CloseToPlayer();
        isRevive = false;
    }
    private void OnStartKillPlayer()
    {
        agent.isStopped = true;
        Get<EnemyAnimation>().StandAnim();
        Get<EnemyStatusUI>().changeStatus = true;
        OnStartStand();
        player.GetComponent<PlayerMovement>().PlayerDeathAnim();
    }
    
    private void CloseToPlayer()
    {
        if(distanceToPlayer < rangeKill)
        {
            DetectPlayer();
        }
        else
            currentKillTime = 0f;
    }

    private void DetectPlayer()
    {
        //Transform playerTransform = PlayerScript.Get<Transform>();
        agent.isStopped = true;    
        currentKillTime += 1f * Time.deltaTime;
        Get<EnemyAnimation>().ActackAnim();
        if (currentKillTime >= timeRequireToKill)
        {
            KillPlayer();
        }

    }

    private void KillPlayer()
    {
        SwitchState(EnemyState.KillPlayer);    
        GameManager.Instance.LoseGame();
    }
    public void RevivePlayer()
    {
        isRevive = true;
        transform.position = defaultPosition;
        Get<EnemyStatusUI>().changeStatus = true;
        Get<EnemyStatusUI>().DisableKillStateUI();
    }
    public void UpdateNewSpeedMove() {
        speedMove += 0.5f;
        if(speedMove >= PlayerScript.Get<PlayerMovement>().Speed -2)
            speedMove = PlayerScript.Get<PlayerMovement>().Speed -2;
    }
    public void MoveTo(Vector3 position)
    {
        Get<EnemyAnimation>().WalkAnim();
        SwitchState(EnemyState.Move);
        destination = position;
        // Code

        //
    }
  
    public void MoveTo(Transform obj)
    {
        MoveTo(obj.position);
    }

    public void ChasePlayer()
    {
        MoveTo(PlayerScript.Get<Transform>());
    }
    private void FaceToFacePlayer()
    {
        transform.LookAt(playerLookAtTarget.transform.position);
        float rotY = transform.eulerAngles.y;
        transform.eulerAngles = new Vector3 (0, rotY, 0);
    } 
    // Duy: add temporary to build test
    public void Stop()
    {
        agent.isStopped = true;
        foreach (var script in GetComponents<MonoBehaviour>())
        {
            script.enabled = false;
        }
    }
    public void Play()
    {
        agent.isStopped = false;
        foreach (var script in GetComponents<MonoBehaviour>())
        {
            script.enabled = true;
        }
    }
    //
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player"))
        {
            player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX 
            | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
        }
    }
    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player"))
        {
            player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX 
            | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
#endif
}
