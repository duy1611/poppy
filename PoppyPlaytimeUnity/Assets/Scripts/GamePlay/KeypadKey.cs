﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadKey : MonoBehaviour
{
    public string key;
    private KeypadController keyPad;

    private void Awake()
    {
        keyPad = transform.GetComponentInParent<KeypadController>();
    }

    public void SendKey()
    {
        keyPad.KeypadTouch(key);
    }
}
