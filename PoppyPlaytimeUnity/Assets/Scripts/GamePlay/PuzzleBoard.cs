﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class PuzzleBoard : MonoBehaviour
{
    public List<PuzzleItem> requireItems;
    [SerializeField] private List<Transform> slotPositions;
    [SerializeField] public List<GameObject> light1;
    [SerializeField] public List<GameObject> lightOfLamp;
    [SerializeField] public GameObject lightOfPlayer;
    public UnityEvent OnCompletePuzzle;

    private Dictionary<PuzzleItem, bool> checkList;

    [SerializeField] private Material greenButton;
    [Header("UI Tutorial")]
    [SerializeField] private GameObject handCursor;
    [SerializeField] private GameObject interfaceBtn;
    [SerializeField] private GameObject headArrow;
    [SerializeField] private string textTutorialWhenClosed;
    private Vector3 defaultPositionOfHandCursor;
    private int indexOjb;

    private void Awake()
    {
        checkList = new Dictionary<PuzzleItem, bool>();
        foreach (var item in requireItems)
        {
            checkList.Add(item, false);
        }

        // to do: change later, hard code for now
        // OnCompletePuzzle.AddListener(GameManager.Instance.WinGame);
    }
    private void Start() {
        defaultPositionOfHandCursor = handCursor.transform.position;
    }
    protected virtual void OnFillPuzzleBoard(PuzzleItem item)
    {
        item.transform.parent = transform;
        item.transform.position = slotPositions[requireItems.IndexOf(item)].position;       
        item.OnPutOnBoard();
        //(Pha): thêm rotation + đổi material từ đỏ sang xanh khi lắp vào
        item.transform.rotation = slotPositions[requireItems.IndexOf(item)].rotation;
        if(light1.Count >0 ) light1[requireItems.IndexOf(item)].GetComponent<Renderer>().material = greenButton;        
    }

    public void FillPuzzleBoard(PuzzleItem item)
    {
        OnFillPuzzleBoard(item);
        checkList[item] = true;
        UI_FuseCheck.Instance?.ShowCheckedFusesUI(item);
        CheckComplete();
    }

    private bool CheckComplete()
    {
        foreach (var item in requireItems)
        {
            if (checkList[item] == false)
                return false;
        }

        OnComplete();
        return true;
    }

    private void OnComplete()
    {
        OnCompletePuzzle?.Invoke();
        //map3: Thêm để mở đèn phòng và tắt đèn player
        TurnOnLightOfLamp();
        TurnOffLightOfPlayer();
        // 
        RemoveThisGameObjectInListArrow();
        SoundManager.Instance.PlayClickSound(); 
        if(ArrowDirection.Instance.listImageObject.Count == 1)
            GameManager.Instance.WinGame();
        else
            GameManager.Instance.MissionCompeted();
        Debug.Log("Done");
    }
    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player")
        {
            StartCoroutine(requireItems[0].GetComponent<PuzzleItem>().ShowTutorialPickUpText());
            UI_Inventory.Instance.pickUpText.GetComponent<Text>().text = textTutorialWhenClosed;
            // if(headArrow) headArrow.SetActive(false);
            if(handCursor && interfaceBtn)
            {
                interfaceBtn.SetActive(true);
                StartCoroutine(ShowHandCursor());
            }
            else return ;
        }
    }
    private void OnTriggerExit(Collider other) {
        if(other.gameObject.tag == "Player")
        {
            UI_Inventory.Instance.pickUpText.SetActive(false);
            // if(headArrow) headArrow.SetActive(true);
            if(handCursor && interfaceBtn)
            {
                StartCoroutine(HideHandCursor());
                interfaceBtn.SetActive(false);
            }
            else return ;
        }
    }
    private IEnumerator ShowHandCursor()
    {
        yield return new WaitForSeconds(0.25f);
        handCursor.SetActive(true);
        Vector3 positionOFHandCursor = new Vector3(interfaceBtn.transform.position.x +4f, interfaceBtn.transform.position.y +4f, interfaceBtn.transform.position.z);
        handCursor.transform.DOMove(positionOFHandCursor,0.5f,false);
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(HideHandCursor());
    }
    private IEnumerator HideHandCursor()
    {
        yield return new WaitForSeconds(0.05f);
        handCursor.transform.DOMove(defaultPositionOfHandCursor,1f,false);
        handCursor.SetActive(false);
    }
    private void TurnOnLightOfLamp()
    {
        if(lightOfLamp.Count!=0)
        {
            foreach (var item in lightOfLamp)
            {
                item.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }
    private void TurnOffLightOfPlayer()
    {
        if(lightOfPlayer)
            lightOfPlayer.SetActive(true);
    }
    private void RemoveThisGameObjectInListArrow()
    {
        if(ArrowDirection.Instance.ojbDirections.Count > 1)
        {
            for(int i =0; i< ArrowDirection.Instance.ojbDirections.Count; i++)
            {
                if(ArrowDirection.Instance.ojbDirections[i] == gameObject)
                {
                    indexOjb = i;
                    ArrowDirection.Instance.ojbDirections.RemoveAt(i);
                }
            }
            for (int i = 0; i < ArrowDirection.Instance.listImageObject.Count; i++)
            {
                if(i == indexOjb)
                {
                    ArrowDirection.Instance.listImageObject.RemoveAt(indexOjb);
                }
            }
        }
    }
    
}
