using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorOpenMap2 : MonoBehaviour
{

    private Vector3 defautPostion, openPosition;
    private void Start() {
        defautPostion = transform.position;
        openPosition = new Vector3(transform.position.x,transform.position.y, transform.position.z + 5f);
    }
    public void OpenTheDoorMap2()
    {
        StartCoroutine(OpenTheDoor());
    }

    private IEnumerator OpenTheDoor()
    {
        transform.DOMove(openPosition,2f,false);
        yield return new WaitForSeconds(0.25f);
        gameObject.SetActive(false);
        yield return new WaitForSeconds(0.15f);
    }
}
