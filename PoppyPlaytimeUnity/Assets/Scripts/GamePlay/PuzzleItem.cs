using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class PuzzleItem : MonoBehaviour
{
    public ItemID ID;
    public bool IsReusable;
    private bool isPickUp;
    
    private void Start() {
        isPickUp = true;
        
    }
       
    public void OnAddToInventory()
    {
        isPickUp = false;
        UI_Inventory.Instance.pickUpText.SetActive(false);

        var outline = GetComponent<OutlineObject>();
        if (outline != null)
            outline.enabled = false;
    }

    public void OnPutOnBoard()
    {
        isPickUp = false;
        gameObject.SetActive(true);
    }
    public IEnumerator ShowTutorialPickUpText()
    {
        yield return new WaitForSeconds(0.05f);
        UI_Inventory.Instance.pickUpText.transform.DOScale(Vector3.zero,0.05f);
        UI_Inventory.Instance.pickUpText.SetActive(true);
        UI_Inventory.Instance.pickUpText.transform.DOScale(Vector3.one,0.5f);
    }
    private void OnTriggerEnter(Collider other) {
        if(isPickUp)
        {
            UI_Inventory.Instance.pickUpText.GetComponent<Text>().text = "Pick up the Toy by Hand";
            StartCoroutine(ShowTutorialPickUpText());
        }
    }
    private void OnTriggerExit(Collider other) {
         UI_Inventory.Instance.pickUpText.SetActive(false);
    }
}
