using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuseShowOrHide : MonoBehaviour
{
    [SerializeField] private List<GameObject> listFuses;
    // Start is called before the first frame update
    void Start()
    {
         if(listFuses.Count != 0)
        {
            for (int i = 0; i < listFuses.Count; i++)
            {
                if(i == 0)
                    listFuses[i].SetActive(true);
                else
                    listFuses[i].SetActive(false);
            }
        }
    }
    public void UpdateActiveFuse() {
        listFuses.RemoveAt(0);
       if(listFuses.Count != 0)
        {
            for (int i = 0; i < listFuses.Count; i++)
            {
                if(i == 0)
                    listFuses[i].SetActive(true);
                else
                    listFuses[i].SetActive(false);
            }
        }
    }
}
