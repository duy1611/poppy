﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class KeypadController : MonoBehaviour
{
    public string password;
    public int passwordLimit;
    public Text passwordText;
    [SerializeField] private GameObject arrowHead;
    [SerializeField] private GameObject PassShowPanel;
    [SerializeField] private List<DoorOpenMap2> listDoorsOpen;
    // [SerializeField] private GameObject pickupText;
    //[SerializeField] private Renderer[] passLights;
    [SerializeField] private TextMeshProUGUI[] passcodeTextOnPaper;
    [SerializeField] private GameObject[] paperPasscode;

    [SerializeField] internal Color[] paperColors;

    //private int currentLight = 0;
    
    private void Start()
    {
        passwordText.text = "";
        // Khoa: thêm vào để set từng số password trên tờ rơi
        if(passcodeTextOnPaper.Length > 0)
            SetPasswordInPaper();
        // 
    }

    public void KeypadTouch(string key)
    {
        if (key == "Clear")
        {
            Clear();
            return;
        }
        else if (key == "Enter")
        {
            Enter();
            return;
        }

        int length = passwordText.text.Length;
        if (length < passwordLimit)
        {
            int number;
            if(int.TryParse(key,out number))
            {
                InputKey(number);
            }
            else
            {
                Debug.Log("Key is invalid: " + key);
            }
        }
    }

    private void InputKey(int number)
    {
        passwordText.text = passwordText.text + number;
        //passLights[currentLight].material.color = keyColors[number];
        //currentLight++;
    }

    public void Clear()
    {
        passwordText.text = "";
        passwordText.color = Color.white;
        //currentLight = 0;

        //foreach (var light in passLights)
        //{
        //    light.material.color = Color.white;
        //}
    }

    private void Enter()
    {
        if (passwordText.text == password)
        {
            passwordText.color = Color.green;
            StartCoroutine(waitAndClear(true));
        }
        else
        {
            passwordText.color = Color.red;
            StartCoroutine(waitAndClear());
        }
    }

    IEnumerator waitAndClear(bool isWin = false)
    {
        yield return new WaitForSeconds(0.75f);
        Clear();
        if (isWin)
        {
            foreach (var item in listDoorsOpen)
            {
                item.OpenTheDoorMap2();
            }
            if(ArrowDirection.Instance.ojbDirections.Count > 1 && ArrowDirection.Instance.ojbDirections.Contains(gameObject) )
            {
                for (var i = 0; i < ArrowDirection.Instance.ojbDirections.Count; i++)
                {
                    if(ArrowDirection.Instance.ojbDirections[i] == gameObject)
                    {
                        ArrowDirection.Instance.listImageObject.RemoveAt(i);
                        ArrowDirection.Instance.ojbDirections.RemoveAt(i);
                    }
                }
            }
            if(PassShowPanel)
                PassShowPanel.SetActive(false);
            GameManager.Instance.MissionCompeted();
        }
    }
    private void SetPasswordInPaper()
    {
        int passwordTemp = int.Parse(password);
        int divive = 1000;
        for (int i = 0; i < password.Length ; i++)
        {
            passcodeTextOnPaper[i].text = (passwordTemp / divive).ToString();
            SetColorOfPaper(i);
            passwordTemp -= (passwordTemp / divive) * divive ;
            divive /= 10;
        }
    }
    private void SetColorOfPaper(int index)
    {
        if(passcodeTextOnPaper[index].text != "")
        {
            for(int i = 0; i < paperColors.Length ; i++)
            {
                if(int.Parse(passcodeTextOnPaper[index].text) == i) 
                    paperPasscode[index].GetComponent<Renderer>().material.color = paperColors[i];
            }
        }
    }
    private void OnTriggerEnter(Collider other) {
        
        if(other.CompareTag("Player"))
        {
            if(arrowHead)
                arrowHead.SetActive(false);
            if(UI_Inventory.Instance.pickUpText)
            {
                UI_Inventory.Instance.pickUpText.SetActive(true);
                UI_Inventory.Instance.pickUpText.GetComponent<Text>().text = "Fill 4 passcode from 4 papers then press E";
            }
        }
    }
    private void OnTriggerExit(Collider other) {
        
        if(other.CompareTag("Player"))
        {
            if(arrowHead)
                arrowHead.SetActive(true);
            if(UI_Inventory.Instance.pickUpText)
                UI_Inventory.Instance.pickUpText.SetActive(false);
        }
    }
}
