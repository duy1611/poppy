using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ArrowDirection : MonoSingleton<ArrowDirection>
{
    public List<GameObject> ojbDirections;
    public List<Sprite> listImageObject;
    public GameObject arrowHead;
    public GameObject notifyArrowPanel;
    public GameObject imageObject;
    public List<GameObject> objectEndMision;
    public List<string> objectEndMisionText;
    [SerializeField] private Transform imageObjectDefault;
    public string textOnStart;
    public string textOnCompleted;
    public string textOnLastCompleted;
    internal int indexOjb;
    Vector3 imageObjectDefaultPosition, imageObjectDefaultScale;
    string textOnCompletedDefaut;
    
    protected override void Awake()
    {
        base.Awake();
    }
    private void Start() {
        indexOjb = 0;
        ShowStatusDirectionWithText(textOnStart);
        imageObject.SetActive(false);
        imageObjectDefaultPosition = imageObjectDefault.position;
        imageObjectDefaultScale = imageObject.transform.localScale;
        textOnCompletedDefaut = textOnCompleted;
    }
    private void Update() {
        LookAtOjbDirection();
    }
    private void LookAtOjbDirection()
    {
        transform.LookAt(ojbDirections[indexOjb].transform.position);
    }
    public void ShowStatusDirectionWithText(string txt)
    {
        if(imageObject)
        {
            if(listImageObject.Count > 0)
            {
                imageObject.GetComponent<Image>().sprite = listImageObject[0];
                StartCoroutine(ShowImageOjbect());
            }
        }
        if(notifyArrowPanel)
        {
            notifyArrowPanel.GetComponent<Animator>().Play("NotifyArrow");
            notifyArrowPanel.GetComponentInChildren<TextMeshProUGUI>().text = txt;
            ArrowDirection.Instance.UpdateTextOjbEndMission();
        }
    }
    public void HideArrow()
    {
        arrowHead.SetActive(false);
    }
    public void ShowArrow()
    {
        arrowHead.SetActive(true);
    }
    private IEnumerator ShowImageOjbect()
    {
        imageObject.SetActive(false);
        yield return new WaitForSeconds(0.15f);
        imageObject.transform.DOScale(Vector3.zero, 0.05f);
        imageObject.SetActive(true);
        imageObject.transform.DOMove(imageObject.transform.parent.GetComponent<Transform>().position, 0.05f);
        imageObject.transform.DOScale(Vector3.one, 0.5f);
        yield return new WaitForSeconds(2f);
        imageObject.transform.DOScale(imageObjectDefaultScale, 0.25f);
        imageObject.transform.DOMove(imageObjectDefaultPosition, 0.15f);
    }
    private void UpdateTextOjbEndMission()
    {
        if(objectEndMision.Count > 0 && objectEndMisionText.Count > 0)
        {
            if(objectEndMision[0] == ojbDirections[0])
            {
                notifyArrowPanel.GetComponentInChildren<TextMeshProUGUI>().text = objectEndMisionText[0];
                objectEndMision.RemoveAt(0);
                objectEndMisionText.RemoveAt(0);
            }
            // else
            //     notifyArrowPanel.GetComponentInChildren<TextMeshProUGUI>().text = textOnCompletedDefaut;

        }
    }

}
