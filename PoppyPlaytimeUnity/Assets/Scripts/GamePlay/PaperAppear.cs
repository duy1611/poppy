using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class PaperAppear : PuzzleItem
{
    public List<TextMeshProUGUI> listPasscodeTexts;
    private Vector3 defautPosition, defautRotation, defautScale;
    private GameObject paper;
    private List<GameObject> paperDirections;
    public GameObject paperApppearancePanel;
    public Button btnContinue;
    internal List<TextMeshProUGUI> listText;
    private void Start() {
        defautPosition = transform.position;
        defautRotation = new Vector3 (transform.rotation.x, transform.rotation.y, transform.rotation.z);
        defautScale = transform.localScale;

        if (paperDirections != null)
            return;

        paperDirections = new List<GameObject>();
        foreach (var item in ArrowDirection.Instance.ojbDirections)
        {
            if(item.GetComponent<PaperAppear>() != null || item.GetComponent<KeypadController>() != null)
                paperDirections.Add(item);
        }
        btnContinue = GameManager.Instance.btnNextMission;
        foreach (var item in paperDirections)
        {
            item.transform.GetChild(0).gameObject.SetActive(false);
            item.GetComponent<BoxCollider>().enabled = false;
        }
        paperDirections[0].transform.GetChild(0).gameObject.SetActive(true);
        paperDirections[0].GetComponent<BoxCollider>().enabled = true;
        // Add Outline
        if (!ArrowDirection.Instance.ojbDirections[0].GetComponent<OutlineObject>())
            ArrowDirection.Instance.ojbDirections[0].AddComponent<OutlineObject>().OutlineWidth = 3f;
    }    
    private void OnTriggerEnter(Collider other) {
       
        if(other.CompareTag("Player"))
        {
            SoundManager.Instance.PlayOpenBookSound();
            ArrowDirection.Instance.HideArrow();
            for(int i  = 0 ; i < paperDirections.Count - 1; i++)
            {
                if(ArrowDirection.Instance.ojbDirections[i] == gameObject)
                {
                    StartCoroutine(ShowPasscodeText(i));
                    ArrowDirection.Instance.indexOjb = i;
                    // if(i == paperDirections.Count - 1)
                    //     ArrowDirection.Instance.indexOjb = 0;
                    ArrowDirection.Instance.ojbDirections.RemoveAt(i);
                    ArrowDirection.Instance.listImageObject.RemoveAt(i);
                }
            }  

            paperApppearancePanel.GetComponent<UI_PaperAppearance>().ShowPaperAppearancePanel();
            UI_Inventory.Instance.pickUpText.GetComponent<Text>().text = "Remember the passcode to fill the KeyPad";
            UI_Inventory.Instance.pickUpText.SetActive(true);
            PlayerScript.Get<PlayerInventory>().AddItem(this);
        }
    }
    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player"))
        {
           gameObject.SetActive(false);
        }
    }
  
    public void DestroyPaper()
    { 
        // string txtOncompleted = ArrowDirection.Instance.textOnCompleted;
        // string txtOnLastcompleted = ArrowDirection.Instance.textOnLastCompleted;
        string txtOncompleted = "Way to the next paper";
        if(UI_Inventory.Instance.pickUpText)
            UI_Inventory.Instance.pickUpText.SetActive(false);
            
        if(ArrowDirection.Instance.ojbDirections[0] == paperDirections[paperDirections.Count-1])
            ArrowDirection.Instance.ShowStatusDirectionWithText("Way to the keypab");
        else
            ArrowDirection.Instance.ShowStatusDirectionWithText(txtOncompleted);

        ArrowDirection.Instance.ShowArrow();
        paperApppearancePanel.GetComponent<UI_PaperAppearance>().HidePaperAppearancePanel();
        SoundManager.Instance.PlayClickSound();
        ArrowDirection.Instance.ojbDirections[0].transform.GetChild(0).gameObject.SetActive(true);
        ArrowDirection.Instance.ojbDirections[0].GetComponent<BoxCollider>().enabled = true;
        if(ArrowDirection.Instance.ojbDirections.Count == 1)
            ArrowDirection.Instance.ojbDirections[0].SetActive(true);
    }

    private IEnumerator ShowPasscodeText(int index)
    {
        listPasscodeTexts[index].transform.DOScale(Vector3.zero,0.05f);
        yield return new WaitForSeconds(0.05f);
        var childText = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        listPasscodeTexts[index].text = childText[childText.Length -1].text;
        listPasscodeTexts[index].transform.DOScale(new Vector3(1.5f,1.5f,1.5f),1f);
        listPasscodeTexts.RemoveAt(index);
        foreach (var item in gameObject.transform.parent.GetComponentsInChildren<PaperAppear>())
        {
            item.listPasscodeTexts = listPasscodeTexts;
        }
    }
}
