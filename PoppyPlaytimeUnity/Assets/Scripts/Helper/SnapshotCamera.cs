using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
[CustomEditor(typeof(SnapshotCamera))]
public class SnapshotCameraEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SnapshotCamera script = (SnapshotCamera)target;

        if (GUILayout.Button("Snapshot"))
        {
            script.CaptureObjects();
        }
    }
}
#endif

[RequireComponent(typeof(Camera))]
public class SnapshotCamera : MonoBehaviour
{
    private Camera snapCam;
    private Camera SnapCam
    {
        get
        {
            if (!snapCam)
            {
                // Init Camera
                snapCam = GetComponent<Camera>();
                snapCam.clearFlags = CameraClearFlags.Depth;
                snapCam.enabled = false;
            }

            return snapCam;
        }
    }

    [Header("Resolution")]
    [SerializeField] private int resWidth = 1024;
    [SerializeField] private int resHeight = 1024;

    [Header("Placement")]
    [SerializeField] private float distance = 2;
    [SerializeField] private Vector3 rotation;

    [Header("Settings")]
    [SerializeField] private bool calculateChildRenderer = false;
    [SerializeField] private bool removeChildWhenDone = true;
    [SerializeField] private int CullingLayer = 31;

    [Header("Targets")]
    [SerializeField] private List<GameObject> targets;

    private int camDepth = 24;
    private string path = "Snapshot";

    private Texture2D SnapTexture()
    {
        // The Render Texture in RenderTexture.active is the one
        // that will be read by ReadPixels.
        var currentRT = RenderTexture.active;
        var newRT = new RenderTexture(resWidth, resHeight, camDepth, RenderTextureFormat.ARGB32);
        SnapCam.targetTexture = newRT;
        RenderTexture.active = newRT;

        // Render the camera's view.
        SnapCam.Render();

        // Make a new texture and read the active Render Texture into it.
        Texture2D image = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);
        image.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        image.Apply();

        // Replace the original active Render Texture.
        RenderTexture.active = currentRT;
        return image;
    }

    public void Snapshot(string dir, string name = "Untitled")
    {
        // Take picture
        var screenShot = SnapTexture();
        byte[] bytes = ImageConversion.EncodeToPNG(screenShot);

        // Save picture
        string filename = Path.Combine(dir, name + ".png");
        File.WriteAllBytes(filename, bytes);

        // Debug
        Debug.Log(string.Format("Took snapshot to: {0}", filename));
    }

    public void CaptureObjects()
    {
        SnapCam.cullingMask = 1 << CullingLayer;

        // Make sure no child remains
        while (transform.childCount != 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }

        if(targets == null || targets.Count == 0)
        {
            Debug.LogError("List target is empty");
            return;
        }

        // Create folder
        string newDir = Path.Combine(path, System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        if (!File.Exists(newDir))
            Directory.CreateDirectory(newDir);

        // Create rig object to rotate clone
        Transform cloneRig = new GameObject("cloneRig").transform;
        cloneRig.parent = transform;
        cloneRig.localPosition = Vector3.forward * distance;
        cloneRig.eulerAngles = rotation;

        // Create and capture clone of objects
        for (int i = 0; i < targets.Count; i++)
        {
            var target = targets[i];
            string name = i + "_" + target.name;

            var clone = Instantiate(target, cloneRig.transform);

            // Move clone to center of rig
            clone.transform.localRotation = Quaternion.identity;
            clone.transform.localPosition -= cloneRig.InverseTransformPoint(GetCoverBounds(clone).center);

            // Change layer
            foreach (var item in clone.GetComponentsInChildren<Transform>())
            {
                item.gameObject.layer = CullingLayer;
            }

            Snapshot(newDir, name);

            if (removeChildWhenDone)
                DestroyImmediate(clone);
        }

        // Remove rig object
        if(removeChildWhenDone)
            DestroyImmediate(cloneRig.gameObject);
    }

    private Bounds GetCoverBounds(GameObject target)
    {
        // Get the bounds that cover whole object
        var renderers = calculateChildRenderer ? target.GetComponentsInChildren<Renderer>() : target.GetComponents<Renderer>();
        Bounds result = renderers[0].bounds;
        for (int i = 1; i < renderers.Length; i++)
        {
            result.Encapsulate(renderers[i].bounds);
        }
        return result;
    }
}
