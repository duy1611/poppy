using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Helper : MonoBehaviour
{
    public static void Log(params object[] messages)
    {
        string msg = messages[0].ToString();
        for (int i = 1; i < messages.Length; i++)
        {
            msg += " - " + messages[i];
        }
        Debug.Log(msg);
    }

    public static bool IsPointOverUIObject(Vector2 pos)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = pos;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public static void PauseGame(bool isPause)
    {
        if (isPause)
        {
            Time.timeScale = 0f;
            AudioListener.pause = true;
        }
        else
        {
            Time.timeScale = 1f;
            AudioListener.pause = false;
        }
    }
}
