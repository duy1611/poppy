using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTouch
{
    public int ID;
    public Vector2 OriginalPos;
    public Vector2 CurrentPos;
    public Vector2 PreviousPos;
    public TouchPhase Phase;

    public static ScreenTouch NewTouch(int ID, Vector2 pos)
    {
        ScreenTouch touch = new ScreenTouch();
        touch.ID = ID;
        touch.OriginalPos = pos;
        touch.CurrentPos = pos;
        touch.PreviousPos = pos;
        touch.Phase = TouchPhase.Began;
        return touch;
    }

    public void UpdateTouch(Vector2 pos)
    {
        PreviousPos = CurrentPos;
        CurrentPos = pos;

        if(PreviousPos != CurrentPos)
        {
            Phase = TouchPhase.Moved;
        }
        else
        {
            Phase = TouchPhase.Stationary;
        }
    }

    public void EndTouch(Vector2 pos)
    {
        PreviousPos = CurrentPos;
        CurrentPos = pos;

        Phase = TouchPhase.Ended;
    }
}
