using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class BackMenu : MonoSingleton<BackMenu>
{
    public GameObject menuBackContainer;
    public GameObject backContainer;
    public GameObject volumeSlider;
    public GameObject volumeImage;
    protected override void Awake()
    {
        base.Awake();
        SceneManager.sceneLoaded += OnSceneLoaded;
        volumeSlider.SetActive(false);
        volumeImage.SetActive(false);
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        menuBackContainer.SetActive(false);
        if(GameManager.Instance.isInGamePlay)
            backContainer.SetActive(true);
        else
            backContainer.SetActive(false);
    }

    public void OpenMenuBackContainer()
    {
        EnemyScript.Get<EnemyMovement>()?.Stop();
        PlayerScript.Get<PlayerMovement>()?.Stop();
        StartCoroutine(ShowMenuBackContainer());
    }
    public void CloseMenuBackContainer()
    {
        EnemyScript.Get<EnemyMovement>()?.Play();
        PlayerScript.Get<PlayerMovement>()?.Play();
        StartCoroutine(HideMenuBackContainer());
    }
    private IEnumerator ShowMenuBackContainer()
    {
        menuBackContainer.transform.DOScale(Vector3.zero,0.05f);
        volumeImage.transform.DOScale(Vector3.zero,0.05f);
        volumeSlider.transform.DOScale(Vector3.zero,0.05f);
        if(menuBackContainer) menuBackContainer.SetActive(true);
        menuBackContainer.transform.DOScale(Vector3.one,0.75f);
        volumeImage.transform.DOScale(Vector3.one,0.75f);
        volumeSlider.transform.DOScale(Vector3.one,0.75f);
        volumeSlider.SetActive(true);
        volumeImage.SetActive(true);
        yield return new WaitForSeconds(0.05f);
    }
    private IEnumerator HideMenuBackContainer()
    {
        menuBackContainer.transform.DOScale(Vector3.zero,1f);
        yield return new WaitForSeconds(0.05f);
        if(menuBackContainer) menuBackContainer.SetActive(false);
        volumeSlider.SetActive(false);
        volumeImage.SetActive(false);
    }
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
