using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;

public class UI_RunButton : MonoBehaviour
// , IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private float timeCountRun;
    [SerializeField] private TextMeshProUGUI timeTextRun;
    [SerializeField] private GameObject timeRunCountContainer;
    [SerializeField] private Image imageOfButtonRun;
    [SerializeField] private Color colorWhenClicked;
    [SerializeField] private GameObject runBtnAds;

    private Color defauftColor;
    private float timeTemp;
    private bool pointerDown;
    private bool isClicked;
    private void Start() {
        defauftColor = imageOfButtonRun.color;
        timeRunCountContainer.SetActive(false);
    }
    private void Update() {
        
        UpdateTimeRun();
        if(pointerDown)
            PlayerScript.Get<PlayerMovement>().ToggleRun(true);
        else
            PlayerScript.Get<PlayerMovement>().ToggleRun(false);

    }
    // public void OnPointerDown(PointerEventData eventData)
    // {
    //     pointerDown = true;
    // }

    // public void OnPointerUp(PointerEventData eventData)
    // {
    //     pointerDown = false;
    // }

    public void BtnRunClick()
    {
        if(!isClicked)
            RunClick();
    }
    private void RunClick()
    {
        timeTemp = timeCountRun;
        StartCoroutine(LockRunButton());
    }
    private void UpdateTimeRun() {    
        if(timeTemp > 0)
            ActiveRun();
        else
            DeactiveRun();
    }
    private void ActiveRun()
    {
        timeTemp -= 1f * Time.deltaTime;
        timeTextRun.text =timeTemp.ToString("00");
        timeRunCountContainer.SetActive(true);
        imageOfButtonRun.color = colorWhenClicked;
        isClicked = true;
        pointerDown = true;
    }
    private void DeactiveRun()
    {
        timeRunCountContainer.SetActive(false);
        imageOfButtonRun.color = defauftColor;
        isClicked = false;
        pointerDown = false;
    }
    private IEnumerator LockRunButton()
    {
        yield return new WaitForSeconds(timeCountRun);
        runBtnAds.SetActive(true);
    }
}
