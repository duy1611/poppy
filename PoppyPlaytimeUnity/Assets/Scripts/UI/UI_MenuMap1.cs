using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_MenuMap1 : MonoBehaviour
{
    //public void Play()
    //{
    //    GameManager.Instance.NextScene();
    //}
   
    public void NextScene()
    {
        MapLoader.Instance.LoadNextScene();
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }

    public void BackToMenu()
    {
        GameManager.Instance.BackToMenu();
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }
}
