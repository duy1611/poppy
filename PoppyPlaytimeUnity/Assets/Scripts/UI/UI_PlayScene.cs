using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_PlayScene : MonoBehaviour
{
    [SerializeField] private GameObject replayButton;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject LoadLevelContainer;
    [SerializeField] internal GameObject nextMisionButton;
    [SerializeField] public Image loadingScreenImg;


    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    private void Start() {
        GameManager.Instance.btnNextMission = nextMisionButton.GetComponent<Button>();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == SceneManager.sceneCountInBuildSettings - 1)
        {
            replayButton.SetActive(true);
            nextButton.SetActive(false);
        }
        else
        {
            replayButton.SetActive(false);
            nextButton.SetActive(true);
        }

    }

    public void Replay()
    {
        GameManager.Instance.ReloadScene();
    }

    public void Next()
    {
        GameManager.Instance.NextScene();
    }

    public void BackToMenu()
    {
        GameManager.Instance.BackToMenu();
    }

    
    public void UpdateLoadingScenePicture()
    {
        if(loadingScreenImg) 
           GameManager.Instance.SetImageOnLoadingScene(loadingScreenImg);
    }
}
