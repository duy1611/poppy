using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI_MenuWaitForPlayGame : MonoBehaviour
{
    public int levelWait;
    public TextMeshProUGUI titleOfChapter;
    public TextMeshProUGUI nameOfChapter;
    public TextMeshProUGUI contentOfChapter;
    public TextMeshProUGUI BtnPlayTextOfChapter;
    public Image imageOfChapter;

    private void Start() {
        for(int i =0 ; i < SaveLoadManager.Instance.listLevel.Count ; i++)
        {
            if(i == levelWait)
            {
                titleOfChapter.text = SaveLoadManager.Instance.listLevel[i].titleLevel;
                nameOfChapter.text = SaveLoadManager.Instance.listLevel[i].nameOfChapter;
                contentOfChapter.text = SaveLoadManager.Instance.listLevel[i].contentOfChapter;
                imageOfChapter.sprite = SaveLoadManager.Instance.listLevel[i].levelImage;
                BtnPlayTextOfChapter.text = "PLAY " + titleOfChapter.text;
            }
        }
    }
}
