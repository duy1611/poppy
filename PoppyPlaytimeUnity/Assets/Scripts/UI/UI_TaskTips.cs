using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UI_TaskTips : MonoSingleton<UI_TaskTips>
{
    [Header("Task Container")]
    public GameObject taskUI;
    public GameObject taskContainer;
    public GameObject bookContainer;
    public GameObject FuseCheckPanel;
    public Image imageTip;
    public TextMeshProUGUI contentTips;
    public Image notifyIcon;
    public Transform buttonContainer;
    public UI_TipButton buttonPrefab;
    private bool isShow = false;
    [System.Serializable] public class TaskTips
    {
        [SerializeField] public string titleTask;
        [SerializeField] public Sprite spriteTask;
        [SerializeField] public string contentTask;
        internal UI_TipButton button;
    }
    [Header("TaskTips List")]
    [SerializeField] public List<TaskTips> listTaskTips;

    private void Start()
    {
        InitButtons();
    }

    private void InitButtons()
    {
        foreach (Transform child in buttonContainer)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < listTaskTips.Count; i++)
        {
            var newButton = Instantiate(buttonPrefab, buttonContainer);
            newButton.Init(i, listTaskTips[i].titleTask);
            listTaskTips[i].button = newButton;
        }

        TipButtonSelected(0);
    }

    public void ToggleTaskContainer()
    {
        if (isShow)
            HideTaskContainer();
        else
            ShowTaskContainer();
    }

    public void ShowTaskContainer()
    {
        isShow = true;

        RectTransform taskContainerTransform = taskContainer.GetComponent<RectTransform>();
        taskContainerTransform.localScale = new Vector3 (0,0,0);
        taskContainerTransform.position = bookContainer.transform.position;
        if(taskContainer) taskContainer.SetActive(true);
        taskContainerTransform.DOMove(taskUI.transform.position ,0.5f);
        taskContainerTransform.DOScale(new Vector3(1,1,1), 0.5f);
        if(FuseCheckPanel) FuseCheckPanel.SetActive(true);
    }

    public void HideTaskContainer()
    {
        StartCoroutine(AnimHideTaskContainer());
    }

    private IEnumerator AnimHideTaskContainer()
    {
        isShow = false;

        RectTransform taskContainerTransform =taskContainer.GetComponent<RectTransform>();
        taskContainerTransform.DOMove(bookContainer.transform.position ,0.5f);
        taskContainerTransform.DOScale(Vector3.zero, 0.5f);
        yield return new WaitForSeconds(0.15f);
        if(taskContainer) taskContainer.SetActive(false);
    }

    public void TipButtonSelected(int idSelect)
    {
        imageTip.sprite = listTaskTips[idSelect].spriteTask;
        contentTips.text = listTaskTips[idSelect].contentTask;
        listTaskTips[idSelect].button.Seen();
        // remove notifyIcon when all tips are seen
        foreach (var tip in listTaskTips)
        {
            if (!tip.button.isSeen)
                return;
        }
        notifyIcon.gameObject.SetActive(false);
    }
  
}
