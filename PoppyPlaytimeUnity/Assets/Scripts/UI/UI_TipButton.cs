using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI_TipButton : MonoBehaviour
{
    public int ID { get; private set; }
    internal bool isSeen { get; private set; } = false;
    [SerializeField] private TextMeshProUGUI Title;
    [SerializeField] private Image notifyIcon;

    public void Init(int ID, string title)
    {
        this.ID = ID;
        Title.text = title;
        notifyIcon.gameObject.SetActive(true);
        GetComponent<Button>().onClick.AddListener(OnButtonClick);
    }

    public void Seen()
    {
        isSeen = true;
        notifyIcon.gameObject.SetActive(false);
    }

    private void OnButtonClick()
    {
        UI_TaskTips.Instance.TipButtonSelected(ID);
        SoundManager.Instance.PlayClickSound();
    }
}
