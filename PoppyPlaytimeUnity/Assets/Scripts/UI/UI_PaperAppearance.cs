using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UI_PaperAppearance : MonoBehaviour
{

    private Vector3 paperAppearancePanelOrginPos;
    private int colorIndex;
    [SerializeField] private GameObject Keypab;
    [SerializeField] private Image imgBackground;
    [SerializeField] private GameObject Arrow;
    [SerializeField] private List<PaperAppear> listPaperAppear;


    // Start is called before the first frame update
    void Start()
    {
        paperAppearancePanelOrginPos = transform.position;
    }

    
    public void ShowPaperAppearancePanel()
    {
        GameManager.Instance.btnNextMission.onClick.AddListener(() => DestroyPaper());
        List<TextMeshProUGUI> listTextMesh = new List<TextMeshProUGUI>();
        foreach (var item in listPaperAppear)
        {
            if(item.gameObject.GetComponent<BoxCollider>().enabled && item.gameObject.activeSelf == true)
               foreach (var textMesh in item.GetComponentsInChildren<TextMeshProUGUI>())
                {   
                    listTextMesh.Add(textMesh);
                }
        }
        colorIndex = int.Parse(listTextMesh[1].text); 
        foreach (var textMesh in GetComponentsInChildren<TextMeshProUGUI>())
        {   
            textMesh.text = listTextMesh[0].text; 
            listTextMesh.RemoveAt(0);
        }
        imgBackground.color = Keypab.GetComponent<KeypadController>().paperColors[colorIndex];
        Transform canvasUI = transform.parent.GetComponent<Transform>();
        transform.DOMove(canvasUI.transform.position, 0.5f, false);
        
    }
    public void HidePaperAppearancePanel()
    {
        transform.DOMove(paperAppearancePanelOrginPos, 0.5f, false);
    }
    public void DestroyPaper()
    {
        foreach (var item in listPaperAppear)
        {
            GameObject paper = item.gameObject;
            if(paper.GetComponent<BoxCollider>().enabled && paper.activeSelf == true)
                paper.GetComponent<PaperAppear>().DestroyPaper();
        }

        // Add Outline to next object
        if (!ArrowDirection.Instance.ojbDirections[0].GetComponent<OutlineObject>())
            ArrowDirection.Instance.ojbDirections[0].AddComponent<OutlineObject>().OutlineWidth = 3f;
    }

    public void CloseButtonClick()
    {
        HidePaperAppearancePanel();
        SoundManager.Instance.PlayClickSound();
    }
}
