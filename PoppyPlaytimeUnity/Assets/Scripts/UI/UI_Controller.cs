using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Controller : MonoBehaviour
{
    [SerializeField] private GameObject AdsRunPanel;
    [SerializeField] private GameObject adsRunBtn;
    [SerializeField] private GameObject buttonAdsInAdsRunPanel;
    [SerializeField] private GameObject buttonCloseAdsInAdsRunPanel;
    private PlayerInteract playerInteract;

    private void Start()
    {
        AdsRunPanel.SetActive(false);
        adsRunBtn.SetActive(true);
        playerInteract = PlayerScript.Get<PlayerInteract>();
    }

    public void LeftHandButton()
    {
        playerInteract.ButtonLeftHandClick();
    }

    public void RightHandButton()
    {
        playerInteract.ButtonRightHandClick();
    }

    public void InteractButton()
    {
        playerInteract.ButtonInteractClick();
    }

    public void AdsRunButtonClick()
    {
        EnemyScript.Get<EnemyMovement>().Stop();
        PlayerScript.Get<PlayerMovement>().Stop();
        StartCoroutine(ShowAdsPanel());
    }
    private IEnumerator ShowAdsPanel()
    {
        AdsRunPanel.SetActive(true);
        buttonCloseAdsInAdsRunPanel.SetActive(false);
        buttonAdsInAdsRunPanel.GetComponent<Animator>().Play("PlayAdsRunBtnScale");
        yield return new WaitForSeconds(4f);
        buttonCloseAdsInAdsRunPanel.SetActive(true);
    }
    public void AdsRunPanelBtnClick()
    {
        GameManager.Instance.ShowAd();
        AdsRunPanel.SetActive(false);
        adsRunBtn.SetActive(false);
        PlayerScript.Get<PlayerMovement>().Play();
        EnemyScript.Get<EnemyMovement>().Play();
    }
    public void HideAdsRunPanel()
    {
        AdsRunPanel.SetActive(false);
        PlayerScript.Get<PlayerMovement>().Play();
        EnemyScript.Get<EnemyMovement>().Play();
    }

}
