using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_FuseCheck : MonoSingleton<UI_FuseCheck>
{
    [Header("Toggle Fuses")]
    public Toggle toggleRedFuse;
    public Toggle toggleYellowFuse;
    public Toggle toggleBlueFuse;
    public Toggle togglePinkFuse;
    [Header("Images Line")]
    public GameObject lineRed;
    public GameObject lineYellow;
    public GameObject lineBlue;
    public GameObject linePink;
    [Header("Pick Labels")]
    public String redLabelPick;
    public String blueLabelPick;
    public String yellowLabelPick;
    public String pinkLabelPick;
    [Header("Completed Labels")]
    public String redLabelCompleted;
    public String blueLabelCompleted;
    public String yellowLabelCompleted;
    public String pinkLabelCompleted;
    [Header("NotifyStatus")]
    public GameObject notifyStatusPanel;
    public List<PuzzleItem> itemsList;
    private TextMeshProUGUI textNotify,textRed,textYellow,textBlue,textPink;

    private void Start() {
        textNotify = notifyStatusPanel.GetComponentInChildren<TextMeshProUGUI>();
        textRed = toggleRedFuse.GetComponentInChildren<TextMeshProUGUI>();
        textBlue = toggleBlueFuse.GetComponentInChildren<TextMeshProUGUI>();
        textYellow = toggleYellowFuse.GetComponentInChildren<TextMeshProUGUI>();
        textPink = togglePinkFuse.GetComponentInChildren<TextMeshProUGUI>();
    }
    
    public void ShowPlayerPickedFusesUI(PuzzleItem item)
    {   
        if(itemsList[0] == item)
            StartCoroutine(PlayerPickUpRedFuse(redLabelPick));
        if(itemsList[1] == item)
            StartCoroutine(PlayerPickUpBlueFuse(blueLabelPick));
        if(itemsList[2] == item)
            StartCoroutine(PlayerPickUpYellowFuse(yellowLabelPick));
        if(itemsList[3] == item)
            StartCoroutine(PlayerPickUpPinkFuse(pinkLabelPick));
    }
    public void ShowCheckedFusesUI(PuzzleItem item)
    {   
        if(itemsList[0] == item)
            ToggleRedFuseIsOn(redLabelCompleted);
        if(itemsList[1] == item)
            ToggleBlueFuseIsOn(blueLabelCompleted);
        if(itemsList[2] == item)
            ToggleYellowFuseIsOn(yellowLabelCompleted);
        if(itemsList[3] == item)
            TogglePinkFuseIsOn(pinkLabelCompleted);  
    }
    private IEnumerator PlayerPickUpRedFuse(string txt)
    {
        SoundManager.Instance.PlayPickSound();
        ToggleIsOn(toggleRedFuse,lineRed);
        // ShowNotifyStatusWithColorAndText(Color.red, txt);
        yield return new WaitForSeconds(2f);
        ToggleIsOff(toggleRedFuse,lineRed);
        textRed.text = txt;
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private void ToggleRedFuseIsOn(string txt)
    {
        textRed.text = txt;
        ToggleIsOn(toggleRedFuse, lineRed);
        ShowNotifyStatusWithColorAndText(Color.red,txt);
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private IEnumerator PlayerPickUpBlueFuse(string txt)
    {
        SoundManager.Instance.PlayPickSound();
        ToggleIsOn(toggleBlueFuse,lineBlue);
        // ShowNotifyStatusWithColorAndText(Color.blue,txt);
        yield return new WaitForSeconds(2f);
        ToggleIsOff(toggleBlueFuse,lineBlue);
        textBlue.text = txt;
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private void ToggleBlueFuseIsOn(string txt)
    {
        textBlue.text = txt;
        ToggleIsOn(toggleBlueFuse,lineBlue);
        ShowNotifyStatusWithColorAndText(Color.blue,txt);
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private IEnumerator PlayerPickUpYellowFuse(string txt)
    {
        SoundManager.Instance.PlayPickSound();
        ToggleIsOn(toggleYellowFuse,lineYellow);
        // ShowNotifyStatusWithColorAndText(Color.yellow,txt);
        yield return new WaitForSeconds(2f);
        ToggleIsOff(toggleYellowFuse,lineYellow);
        textYellow.text = txt;
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private void ToggleYellowFuseIsOn(string txt)
    {
        textYellow.text = txt;
        ToggleIsOn(toggleYellowFuse,lineYellow);
        ShowNotifyStatusWithColorAndText(Color.yellow,txt);
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private IEnumerator PlayerPickUpPinkFuse(string txt)
    {
        SoundManager.Instance.PlayPickSound();
        ToggleIsOn(togglePinkFuse,linePink);
        // ShowNotifyStatusWithColorAndText(new Color(255,0,147,255),txt);
        yield return new WaitForSeconds(2f);
        ToggleIsOff(togglePinkFuse,linePink);
        textPink.text = txt;
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private void TogglePinkFuseIsOn(string txt)
    {
        textPink.text = txt;
        ToggleIsOn(togglePinkFuse,linePink);
        ShowNotifyStatusWithColorAndText(new Color(255,0,147,255), txt);
        SoundManager.Instance.PlaySuscessTaskSoundSound();
    }
    private void ToggleIsOn(Toggle toggle, GameObject lineColor)
    {
        toggle.isOn = true;
        lineColor.SetActive(true);
    }
    private void ToggleIsOff(Toggle toggle, GameObject lineColor)
    {
        toggle.isOn = false;
        lineColor.SetActive(false);
    }
    
    private void ShowNotifyStatusWithColorAndText(Color color, string txt)
    {
        textNotify.faceColor = color;
        textNotify.text = txt;
        notifyStatusPanel.GetComponent<Animator>().SetTrigger("ShowNotifyStatus");
    }


 
}
