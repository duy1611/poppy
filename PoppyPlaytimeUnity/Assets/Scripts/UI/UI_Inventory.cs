using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Inventory : MonoSingleton<UI_Inventory>
{
    [SerializeField] private ItemIconDict itemIconDict;
    [SerializeField] private List<Image> inventorySlots;
    [SerializeField] private List<GameObject> countInventorySlots;
    [SerializeField] internal GameObject pickUpText;
    [SerializeField] private Image imageOfItemCollectPanel;
    [SerializeField] private GameObject colectInventoryPanel;
    [SerializeField] private GameObject AdsWatchBtn;
    [SerializeField] private GameObject CloseBtnInColectInventoryPanel;
    private List<ItemID> inventoryItems;
    private ItemID itemIDTemp;
    protected override void Awake()
    {
        base.Awake();
        inventoryItems = new List<ItemID>();
    }

    public void AddInventoryItemOnUI(ItemID ID)
    {
        inventoryItems.Add(ID);
        itemIDTemp = ID;
        ShowCollectInventory();
    }

    public void RemoveInventoryItemOnUI(ItemID ID)
    {
        inventoryItems.Remove(ID);
        RedrawInventory();
        foreach (var item in countInventorySlots)
        {
            item.SetActive(false);   
        }
    }

    private void RedrawInventory()
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            if(i < inventoryItems.Count)
            {
                inventorySlots[i].enabled = true;
                inventorySlots[i].sprite = itemIconDict[inventoryItems[i]];
                imageOfItemCollectPanel.sprite = itemIconDict[inventoryItems[i]];
            }
            else
            {
                inventorySlots[i].enabled = false;
                inventorySlots[i].sprite = null;
            }
        }
    }
    private void ShowCollectInventory()
    {
        EnemyScript.Get<EnemyMovement>()?.Stop();
        PlayerScript.Get<PlayerMovement>()?.Stop();
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            if(i < inventoryItems.Count)
            {
                imageOfItemCollectPanel.sprite = itemIconDict[inventoryItems[i]];
            }
        }
        StartCoroutine(ShowColectInventoryPanel());
    }
    private IEnumerator ShowColectInventoryPanel()
    {
        yield return new WaitForSeconds(0.5f);
        colectInventoryPanel.SetActive(true);
        CloseBtnInColectInventoryPanel.SetActive(false);
        AdsWatchBtn.GetComponent<Animator>().Play("CollectButtonItemAdsScale");
        yield return new WaitForSeconds(4f);
        CloseBtnInColectInventoryPanel.SetActive(true);
    }
    public void HideCollectInventory()
    {
        colectInventoryPanel.SetActive(false);
        RedrawInventory();
        GameManager.Instance.MissionCompeted();
    }
    private void UpdateCountItem(ItemID id) {
        for (int i = 0; i < countInventorySlots.Count; i++)
        {
            if(i < inventoryItems.Count)
            {
                if(inventoryItems[i] == id)
                    countInventorySlots[i].SetActive(true);
            }
        }
    }
    public void ClickColectInventoryMultiplication()
    {
        RedrawInventory();
        UpdateCountItem(itemIDTemp);
        HideCollectInventory();
        GameManager.Instance.ShowAd();
        GameManager.Instance.MissionCompeted();
    }
}
