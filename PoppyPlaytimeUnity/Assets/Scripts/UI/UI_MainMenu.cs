using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MainMenu : MonoBehaviour
{
    public GameObject LoadContainer;
    public GameObject SettingsContainer;
    private void Start() {
        LoadContainer = GameObject.Find("/Managers/Manager UI/LoadLevel/LoadLevelContainer");
        SettingsContainer = GameObject.Find("/Managers/Manager UI/Settings/SettingContainer"); 
    }
    public void Play()
    {
        GameManager.Instance.NextScene();
        LoadContainer.SetActive(false);
        SettingsContainer.SetActive(false);
    }

    public void Load()
    {
        SettingsContainer.SetActive(false);
        LoadContainer.SetActive(true);
    }

    public void Settings()
    {
        LoadContainer.SetActive(false);
        SettingsContainer.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
