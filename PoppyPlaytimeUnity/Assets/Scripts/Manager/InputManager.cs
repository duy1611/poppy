using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoSingleton<InputManager>
{
    private Joystick joystick;
    private Dictionary<int, ScreenTouch> touches;

    public ScreenTouch CurrentScreenTouch { get; private set; }

    public bool IsPressingJoystick
    {
        get
        {
            return joystick.Direction.magnitude != 0;
        }
    }

    public float Horizontal
    {
        get
        {
            float x = Input.GetAxis("Horizontal");
            if (x == 0f)
                x = joystick.Horizontal;
            return x;
        }
    }

    public float Vertical
    {
        get
        {
            float y = Input.GetAxis("Vertical");
            if (y == 0f)
                y = joystick.Vertical;
            return y;
        }
    }
    
    protected override void Awake()
    {
        base.Awake();

        SceneManager.sceneLoaded += OnSceneLoaded;

        touches = new Dictionary<int, ScreenTouch>();
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Update()
    {
        UpdateTouches();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        joystick = FindObjectOfType<Joystick>();
    }

    private void UpdateTouches()
    {
        // Remove ended touches
        List<int> keys = new List<int>(touches.Keys);
        foreach (var key in keys)
        {
            if(touches[key].Phase == TouchPhase.Ended)
            {
                RemoveTouch(key);
            }
        }

#if UNITY_EDITOR_WIN
        if (Input.GetMouseButtonDown(0))
        {
            AddTouchOverwrite(0, Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touches[0].EndTouch(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            touches[0].UpdateTouch(Input.mousePosition);
        }
#else
        foreach (var touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                AddTouchOverwrite(touch.fingerId, touch.position);
            }
            else if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
            {
                touches[touch.fingerId].EndTouch(touch.position);
            }
            else
            {
                touches[touch.fingerId].UpdateTouch(touch.position);
            }
        }
#endif


    }

    private void AddTouchOverwrite(int ID, Vector2 pos)
    {
        if (touches.ContainsKey(ID))
            RemoveTouch(ID);

        AddTouch(ID, pos);
    }

    
    private void AddTouch(int ID, Vector2 pos)
    {
        var newTouch = ScreenTouch.NewTouch(ID, pos);
        touches.Add(ID, newTouch);
        if (CurrentScreenTouch == null && !Helper.IsPointOverUIObject(pos))
            CurrentScreenTouch = newTouch;
    }

    private void RemoveTouch(int ID)
    {
        touches.Remove(ID);
        if (CurrentScreenTouch != null && CurrentScreenTouch.ID == ID)
            CurrentScreenTouch = null;
    }
}
