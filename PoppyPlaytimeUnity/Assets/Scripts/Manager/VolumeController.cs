using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeController : MonoBehaviour
{
    [Header("Volume In Game")]
    public GameObject volumeSliderContainerInGame;
    public GameObject volumeSliderOjb;
    public Slider volumeSlider;
    public Image imgVolumeInGame;
    [Header("Volume In MenuScene")]
    public Slider volumeSliderInMenu;
    public Image imgVolumeInMenu;
    public Sprite[] imgVolumne;
    public GameObject AudioManager;
    // bool isShowVolumne;
    private void Start() {
        volumeSliderInMenu.value =  SoundManager.Instance.volumne;
    }
    private void Update() {
        // if(GameManager.Instance.isInMenu)
        // {
        //     if(volumeSliderContainerInGame) volumeSliderContainerInGame.SetActive(false);
        //     UpdateVolumneInMenu();
        // }
        if(GameManager.Instance.isInGamePlay)
        {
            if(volumeSliderContainerInGame) volumeSliderContainerInGame.SetActive(true);
            UpdateVolumneInGame();  
        }
        else
        {
            if(volumeSliderContainerInGame) volumeSliderContainerInGame.SetActive(false);
        }
    }
    private void UpdateVolumneInGame()
    {
        SoundManager.Instance.volumne = volumeSlider.value;
        volumeSliderInMenu.value = volumeSlider.value;
        if(volumeSlider.value > .6)
            imgVolumeInGame.sprite = imgVolumne[0];
        else if(volumeSlider.value < .6 && volumeSlider.value > 0)
            imgVolumeInGame.sprite = imgVolumne[1];
        else
            imgVolumeInGame.sprite = imgVolumne[2];
    }
    private void UpdateVolumneInMenu()
    {
        SoundManager.Instance.volumne = volumeSliderInMenu.value;
        volumeSlider.value = volumeSliderInMenu.value;
        if(volumeSliderInMenu.value > .6)
            imgVolumeInMenu.sprite = imgVolumne[0];
        else if(volumeSliderInMenu.value < .6 && volumeSliderInMenu.value > 0)
            imgVolumeInMenu.sprite = imgVolumne[1];
        else
            imgVolumeInMenu.sprite = imgVolumne[2];
    }
    // public void ClickVolumneControl()
    // {
    //     if(isShowVolumne)
    //     {
    //         HideVolumneControl();
    //         return ;
    //     }
    //     if(!isShowVolumne)
    //         ShowVolumneControl();
    // }
    // private void ShowVolumneControl()
    // {   
    //     isShowVolumne = true;
    //     volumeSliderOjb.SetActive(true);
    //     SoundManager.Instance.PlayClickSound();
    //     StartCoroutine(HideWaitVolumneControl());
    // }


    // public IEnumerator HideWaitVolumneControl()
    // {
    //     yield return new WaitForSeconds(5f);
    //     isShowVolumne = false;
    //     volumeSliderOjb.SetActive(false);
    // }

    // private void HideVolumneControl()
    // {
    //     isShowVolumne = false;
    //     volumeSliderOjb.SetActive(false);
    //     SoundManager.Instance.PlayClickSound();
    // }
    
    
}
