using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIGamePlayManager : MonoSingleton<UIGamePlayManager>
{
    [Header("GamePlay Panel References")]
    public GameObject completedMisionPanel;
    public GameObject gameOverPanel;
    public GameObject gamePassPanel;
    public GameObject canvasUI;
    public GameObject ReplayBtnInGameOver;
    public GameObject RevivalBtnInGameOver;
    public GameObject loadLevelContainer;
    private Vector3 gameOverOriginalPos;
    private Vector3 gamePassOriginalPos;

    protected override void Awake()
    {
        base.Awake();
        gameOverOriginalPos = gameOverPanel.transform.position;
        gamePassOriginalPos = gamePassPanel.transform.position;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void Start() {
            ShowLoadLevelContainer();   
    }
    private void Update() {
        if(GameManager.Instance.isInMenu)
            ShowLoadLevelContainer();
        else
            HideLoadLevelContainer();
    }
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        gameOverPanel.SetActive(false);
        gamePassPanel.SetActive(false);

        gameOverPanel.transform.position = gameOverOriginalPos;
        gamePassPanel.transform.position = gamePassOriginalPos;
    }

    public IEnumerator ShowGameOverPanel()
    {
        SoundManager.Instance.PlaySoundGameOver();
        gameOverPanel.SetActive(true);
        yield return new WaitForSeconds(0.8f);
        gameOverPanel.transform.DOMove(canvasUI.transform.position,0.5f, false);
        RevivalBtnInGameOver.GetComponent<Animator>().Play("RevivalBtnInGameOverScale");
        StartCoroutine(SoundManager.Instance.StopAllSound());
        StartCoroutine(ShowReplayBtnInGameOver());
    }
    public void HideGameOverPanel()
    {
        gameOverPanel.SetActive(false);
        gameOverPanel.transform.position = gameOverOriginalPos;
    }
    public IEnumerator ShowCompetedMisionPanel()
    {
        // SoundManager.Instance.PlaySoundGameOver();
        completedMisionPanel.SetActive(true);
        yield return new WaitForSeconds(0.8f);
        completedMisionPanel.transform.DOMove(canvasUI.transform.position,0.5f, false);
        StartCoroutine(SoundManager.Instance.StopAllSound());
        
    }
    public IEnumerator ShowReplayBtnInGameOver()
    {
        ReplayBtnInGameOver.SetActive(false);
        yield return new WaitForSeconds(4f);
        ReplayBtnInGameOver.SetActive(true);
    }
    public void HideCompetedMisionPanel()
    {
        completedMisionPanel.SetActive(false);
        completedMisionPanel.transform.position = gameOverOriginalPos;
    }
    public IEnumerator ShowGamePassPanel()
    {
        SoundManager.Instance.PlaySoundGamePass();
        gamePassPanel.SetActive(true);
        yield return new WaitForSeconds(0.8f);
        gamePassPanel.transform.DOMove(canvasUI.transform.position,0.5f, false);
    }
    private void HideLoadLevelContainer()
    {
        if(loadLevelContainer) loadLevelContainer.SetActive(false);
    }
    private void ShowLoadLevelContainer()
    {
        if(loadLevelContainer) loadLevelContainer.SetActive(true);
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
