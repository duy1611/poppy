using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SaveLoadManager : MonoSingleton<SaveLoadManager>
{
    [System.Serializable] public class Level
    {
        [SerializeField] public string titleLevel;
        [SerializeField] public int levelNumber;
        [SerializeField] public bool isPass;
        [SerializeField] public Sprite levelImage;
        [SerializeField] public Image levelImageUI;
        [SerializeField] public TextMeshProUGUI txtlevelUI;
        [SerializeField] public GameObject lockLevelOjb;
        [Header("Informations of Chapter")]
        public string nameOfChapter;
        public string contentOfChapter;
        
    }
    [System.Serializable]
    public class LevelCompleted
    {
        public int level = 0;
    }

    private string levelCompletedKey = "bsdat";
    public LevelCompleted lastLevel = new LevelCompleted();
    public bool isLastLevel = false;
    public int currentLevel;
    public int selectLevel;
    protected override void Awake()
    {
        base.Awake();
        if(BinaryDataStream.Exist(levelCompletedKey))
        {
            StartCoroutine(ReadDataFile());
        }
    }

    [SerializeField] public List<Level> listLevel;   
     private void Start() {
        currentLevel = lastLevel.level;
        for(int i = 0 ; i < listLevel.Count ; i++)
        {
            listLevel[i].txtlevelUI.text = listLevel[i].titleLevel;
            listLevel[i].levelImageUI.sprite = listLevel[i].levelImage;     
        }  
        
    }

    public void UpdateLastestLevel()
    {
        if(currentLevel == selectLevel)
            currentLevel ++;
        if(currentLevel > lastLevel.level)
        {
            isLastLevel = true;
            lastLevel.level = currentLevel;
            SaveLevel(true);
            CheckLockLevel();
        }
    }


    public void CheckLockLevel() {
        for(int i =0 ; i <= lastLevel.level ; i++ )
        {
            listLevel[i].isPass = true;
        }
        for(int i = 0 ; i< listLevel.Count ; i++)
        {
            if(listLevel[i].isPass)
            {
                listLevel[i].lockLevelOjb.SetActive(false);
            }
            else 
                listLevel[i].lockLevelOjb.SetActive(true);
        } 
    }
    public void SetPassLevel(int levelIndex)
    {
        listLevel[levelIndex].isPass = true;
        CheckLockLevel();
    }
    public void GetSelectLevel(int level)
    {
        selectLevel = level - 1;
        if(selectLevel == lastLevel.level)
            isLastLevel = true;
    }
    public void SetCurrentLevel(int level)
    {
        currentLevel = level - 1;
    }
   
    private IEnumerator ReadDataFile()
    {
        lastLevel = BinaryDataStream.Read<LevelCompleted>(levelCompletedKey);
        yield return new WaitForEndOfFrame();
        CheckLockLevel();
    }
    public void SaveLevel(bool newLevel)
    {
        BinaryDataStream.Save<LevelCompleted>(lastLevel,levelCompletedKey);
        CheckLockLevel();
    }

}
