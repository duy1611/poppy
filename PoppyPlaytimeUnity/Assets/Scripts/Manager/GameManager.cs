using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoSingleton<GameManager>
{
    private const int menuScene = 0;
    private int currentScene;
    public bool isInMenu;
    public bool isInGamePlay;
    internal Button btnNextMission;

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        currentScene = SceneManager.GetActiveScene().buildIndex;
        if(currentScene == 0)
        {
            isInMenu = true;
            isInGamePlay = false;
        }
        else
            isInMenu = false;
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
            WinGame();

        PlayerScript.Get<PlayerMovement>()?.ToggleRun(Input.GetKey(KeyCode.LeftShift));

        if (Input.GetKeyDown(KeyCode.Q))
            PlayerScript.Get<PlayerInteract>()?.ButtonLeftHandClick();

        if (Input.GetKeyDown(KeyCode.E))
            PlayerScript.Get<PlayerInteract>()?.ButtonRightHandClick();
    }

    public void LoadScene(int index)
    {
        if(index >= 0 && index < SceneManager.sceneCountInBuildSettings)
        {
            currentScene = index;
            SceneManager.LoadScene(currentScene);
        }
        if(currentScene!= 0 && currentScene % 2 == 0)
            isInGamePlay = true;
        else
            isInGamePlay = false;
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }

    public void NextScene()
    {
        LoadScene(currentScene + 1);
        if(currentScene!= 0 && currentScene % 2 == 0)
            isInGamePlay = true;
        else
            isInGamePlay = false;
       
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }

    public void ReloadScene()
    {
        LoadScene(SceneManager.GetActiveScene().buildIndex);
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }

    public void BackToMenu()
    {
        LoadScene(menuScene);
        isInMenu = true;
        StartCoroutine(SoundManager.Instance.StopAllSound());
    }

    public void LoseGame()
    {
        EnemyScript.Get<EnemyMovement>()?.Stop();
        PlayerScript.Get<PlayerMovement>()?.Stop();
        ShowLosePanel();
        Debug.Log("Game Over");
    }
    public void MissionCompeted()
    {
       EnemyScript.Get<EnemyMovement>()?.Stop();
        PlayerScript.Get<PlayerMovement>()?.Stop();
        ShowCompetedMisionPanel();
    }

    

    public void ReviveGame()
    {
        DisableLosePanel();
        EnemyScript.Get<EnemyMovement>()?.Play();
        PlayerScript.Get<PlayerMovement>()?.Play();
        PlayerScript.Get<PlayerMovement>().PlayerRevive();
        EnemyScript.Get<EnemyMovement>().RevivePlayer();
    }
    public void ContinueMisionGame()
    {
        DisableCompletedMisionPanel();
        EnemyScript.Get<EnemyMovement>()?.Play();
        EnemyScript.Get<EnemyMovement>().RevivePlayer();
        EnemyScript.Get<EnemyStatusUI>().UpdateNewRange();
        EnemyScript.Get<EnemyStatusUI>().UpdateNewTimeWander();
        EnemyScript.Get<EnemyMovement>().UpdateNewSpeedMove();
        PlayerScript.Get<PlayerMovement>()?.Play();
        PlayerScript.Get<PlayerInventory>().ShowStatusDirection();
        PlayerScript.Get<PlayerMovement>().PlayerRevive();
    }
    public void WinGame()
    {
        EnemyScript.Get<EnemyMovement>()?.Stop();
        PlayerScript.Get<PlayerMovement>()?.Stop();
        ShowWinPanel();
        Debug.Log("Win");
    }
    public void ShowAd()
    {
        Debug.Log("ShowAd");
    }

    private void ShowLosePanel()
    {
        StartCoroutine(UIGamePlayManager.Instance.ShowGameOverPanel());
    }
    private void ShowCompetedMisionPanel()
    {
        StartCoroutine(UIGamePlayManager.Instance.ShowCompetedMisionPanel());
    }
    private void DisableLosePanel()
    {
        UIGamePlayManager.Instance.HideGameOverPanel();
    }
    private void DisableCompletedMisionPanel()
    {
        UIGamePlayManager.Instance.HideCompetedMisionPanel();
    }

    private void ShowWinPanel()
    {
        StartCoroutine(UIGamePlayManager.Instance.ShowGamePassPanel());
        
    }
    public void SetImageOnLoadingScene(Image img)
    {
        img.sprite = SaveLoadManager.Instance.listLevel[SaveLoadManager.Instance.selectLevel].levelImage;
    }
    
}
