using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SoundManager : MonoSingleton<SoundManager>
{
    [Header("Audio Sources")]
    public AudioSource Music;
    public AudioSource effectSoundAud;
    public AudioSource UISoundAud;
    public AudioSource clockSoundAud;
    public AudioSource playerSoundAud;
    public AudioSource emenySoundAud;
    [Header("Audio Clips")]
    public AudioClip clockSound;
    public AudioClip LevelComplete;
    public AudioClip LevelFail;
    public AudioClip notificationSound;
    public AudioClip openBookSound;
    public AudioClip clickSound;
    public AudioClip suscessTaskSound;
    public AudioClip playerFootStep;
    public AudioClip playerHandSound;
    public AudioClip enemyFootStep;
    public AudioClip enemyWalkSound;
    public AudioClip enemyActack ;
    public AudioClip backgroundMusic ;
    internal float volumne;
    
    protected override void Awake()
    {
        base.Awake();
        volumne = 1;
        volumne = effectSoundAud.volume;
    }

    public IEnumerator StopAllSound()
    {
        AudioSource[] audioSources = GetComponentsInChildren<AudioSource>();
        yield return new WaitForSeconds(0.25f);
        foreach (var item in audioSources)
        {
            if(item.isPlaying)
                item.Stop();
        }
    }

    public void PlayNotificationSound()
    {
        effectSoundAud.volume = volumne;
        effectSoundAud.clip = notificationSound;
        effectSoundAud.Play();
    }
    public void PlayClockSound()
    {
        clockSoundAud.volume = volumne;
        clockSoundAud.clip = clockSound;
        if(clockSoundAud.isPlaying == false)
            clockSoundAud.Play();
    }
    public void StopClockSound()
    {
        clockSoundAud.volume = volumne;
        clockSoundAud.clip = clockSound;
        clockSoundAud.Stop();
    }
    public void PlayOpenBookSound()
    {
        UISoundAud.volume = volumne;
        UISoundAud.clip = openBookSound;
        UISoundAud.Play();
    }
    public void PlayClickSound()
    {
        UISoundAud.volume = volumne;
        UISoundAud.clip = clickSound;
        UISoundAud.Play();
    }
    public void PlayPickSound()
    {
        UISoundAud.volume = volumne;
        UISoundAud.clip = clickSound;
        UISoundAud.Play();
    }
    public void PlaySuscessTaskSoundSound()
    {
        UISoundAud.volume = volumne;
        UISoundAud.clip = suscessTaskSound;
        UISoundAud.Play();
    }
    public void PlayPlayerFootStepSound()
    {
        playerSoundAud.volume = volumne;
        playerSoundAud.clip = playerFootStep;
        if(playerSoundAud.isPlaying == false)
            playerSoundAud.Play();
    }
    public void StopPlayerFootStepSound()
    {
        playerSoundAud.volume = volumne;
        playerSoundAud.Stop();
    }
    public void PlayPlayerHandSound()
    {
        effectSoundAud.volume = volumne;
        effectSoundAud.clip = playerHandSound;
        effectSoundAud.Play();
    }
    public void PlayEnemyFootStepSound()
    {
        emenySoundAud.volume = volumne;
        emenySoundAud.clip = enemyFootStep;
        if(emenySoundAud.isPlaying == false)
            emenySoundAud.Play();
    }
    public void PlayEnemyWalkSound()
    {
        emenySoundAud.volume = volumne;
        emenySoundAud.clip = enemyWalkSound;
        if(emenySoundAud.isPlaying == false)
            emenySoundAud.Play();
    }
    public void PlaySoundEnemyActack()
    {
        effectSoundAud.volume = volumne;
        effectSoundAud.clip = enemyActack;
        if(effectSoundAud.isPlaying == false)
            effectSoundAud.Play();
    }
    public void PlaySoundGameOver()
    {   
        effectSoundAud.volume = volumne;
        effectSoundAud.clip = LevelFail;
        effectSoundAud.Play();
    }
    public void PlaySoundGamePass()
    {   
        effectSoundAud.volume = volumne;
        effectSoundAud.clip = LevelComplete;
        effectSoundAud.Play();
    }   
    // public void PlayBackgroundMusic()
    // {   
    //     Music.volume = volumne/2;
    //     Music.clip = backgroundMusic;
    //     Music.loop = true;
    //     Music.Play();
    //     Debug.Log("play background");
    // }   
}
