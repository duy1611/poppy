using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerInteract : PlayerScript
{
    [SerializeField] private MachineHand leftHand;
    [SerializeField] private MachineHand rightHand;
    [SerializeField] private float maxDistanceHand;
    [SerializeField] private float maxDistanceInteract;
    private Camera mainCamera;
    private RectTransform crosshair;
    private Vector2 crosshairPos;

    protected override void Awake()
    {
        base.Awake();

        mainCamera = Camera.main;
        crosshair = GameObject.FindGameObjectWithTag("Crosshair").GetComponent<RectTransform>();
        crosshairPos = (new Vector2(Screen.width, Screen.height) / 2f) + crosshair.anchoredPosition;
    }

    public void ButtonInteractClick()
    {
        RaycastHit target = GetTarget(maxDistanceInteract);
        if(target.transform != null)
        {
            PuzzleItem item = target.transform.GetComponent<PuzzleItem>();
            if (item != null)
            {
                Get<PlayerInventory>().AddItem(item);
                return;
            }

            PuzzleBoard board = target.transform.GetComponent<PuzzleBoard>();
            if(board!= null)
            {
                var inventory = Get<PlayerInventory>();
                foreach (var req in board.requireItems)
                {
                    if (inventory.Contain(req))
                    {
                        inventory.UseItem(req);
                        board.FillPuzzleBoard(req);
                    }
                }
                return;
            }
        }
        SoundManager.Instance.PlayPlayerHandSound();
    }

    public void ButtonLeftHandClick()
    {
        leftHand.GetComponentInChildren<Animator>().Play("L_HandTake");
        FireMachineHand(leftHand);
        SoundManager.Instance.PlayPlayerHandSound();
    }

    public void ButtonRightHandClick()
    {
        rightHand.GetComponentInChildren<Animator>().Play("R_HandTake");
        FireMachineHand(rightHand);
        SoundManager.Instance.PlayPlayerHandSound();
    }

    public void FireMachineHand(MachineHand hand)
    {
        RaycastHit target = GetTarget(maxDistanceHand);
        PuzzleItem item = target.transform?.GetComponent<PuzzleItem>();
        if (item != null)
        {
            hand.FireHand(target.point, true, () => StartCoroutine(GrabItem(hand, item.transform)), () => Get<PlayerInventory>().AddItem(item));
        }
        else
            hand.FireHand(target.point);
    }

    private IEnumerator GrabItem(MachineHand machineHand, Transform item)
    {
        item.transform.DOMove(machineHand.transform.position, 0.15f);
        yield return new WaitForSeconds(0.15f);
        item.gameObject.SetActive(false);
        item.parent = machineHand.Hand;
    }

    private RaycastHit GetTarget(float distance)
    {
        RaycastHit hit;

        Ray ray = mainCamera.ScreenPointToRay(crosshairPos);
        if (!Physics.Raycast(ray, out hit, distance))
        {
            hit.point = ray.origin + ray.direction * distance;
        }

        //keypad
        if (hit.transform?.GetComponent<KeypadKey>() != null)
        {
            hit.transform.GetComponent<KeypadKey>().SendKey();
        }

        return hit;
    }
}
