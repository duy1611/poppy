using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PlayerCamera : PlayerScript
{
    public Transform PlayerHead;
    [SerializeField] private float cameraSensitivity;
    [SerializeField] private float cameraAngleLimit = 90f;

    private void Update()
    {
        RotateWithTouch();
    }

    private void ApplyRotation(float x, float y)
    {
        // Rotate head
        float rotationX = PlayerHead.eulerAngles.x + y * cameraSensitivity * Time.deltaTime;
        // Limit vertical rotation
        rotationX = rotationX > 180f ? rotationX - 360f : rotationX;
        rotationX = Mathf.Clamp(rotationX, -cameraAngleLimit, cameraAngleLimit);
        PlayerHead.eulerAngles = new Vector3(rotationX, PlayerHead.eulerAngles.y);

        // Rotate body
        float rotationY = transform.eulerAngles.y + x * cameraSensitivity * Time.deltaTime;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, rotationY);
    }

    private void RotateWithTouch()
    {
        ScreenTouch touch = InputManager.Instance.CurrentScreenTouch;

        if (touch == null)
            return;

        if (touch.Phase == TouchPhase.Moved)
        {
            Vector2 touchMoveDir = (touch.CurrentPos - touch.PreviousPos) / Time.deltaTime / 1000f;
            ApplyRotation(touchMoveDir.x, -touchMoveDir.y / 2f);
        }
    }

    private void RotateWithMouse()
    {
        UpdatePCInput();

        if (!Cursor.visible)
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = -Input.GetAxis("Mouse Y");

            ApplyRotation(mouseX, mouseY);
        }
    }

    private void UpdatePCInput()
    {
        if (Cursor.lockState == CursorLockMode.None && Input.GetMouseButtonDown(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
