using UnityEngine;
using DG.Tweening;

public class PlayerMovement : PlayerScript
{
    [SerializeField] private float speed;
    public float Speed { get => speed; }
    private float runSpeed, walkSpeed;
    [SerializeField] private float gravityForce;
    private float gravityForceLive;
    [SerializeField] private GameObject machineHand;
    public GameObject enemyLookTarget;
    private bool isGround;
    Vector3 defaultPosition,defaultRotation;
    private Animator m_anim;
    private void Start() {
        defaultPosition = gameObject.transform.position;    
        gravityForceLive = gravityForce;
        m_anim = machineHand.GetComponent<Animator>();   
        runSpeed = speed + 7f;
        walkSpeed = speed;
        // SoundManager.Instance.PlayBackgroundMusic();
    }

    private void Update()
    {
        float x = InputManager.Instance.Horizontal;
        float z = InputManager.Instance.Vertical;

        Vector3 v = (transform.right * x + transform.forward * z) * speed;
        Get<Rigidbody>().velocity = new Vector3(v.x, Get<Rigidbody>().velocity.y, v.z);
       
        if(x != 0 || z != 0)
        {
            SoundManager.Instance.PlayPlayerFootStepSound();
            m_anim.Play("DoubleHandRun");
        }        
        GroundCheck();
    }
    public void PlayerDeathAnim()
    {
        FaceToFaceEnemy();
        transform.DOMove(new Vector3(transform.position.x, transform.position.y + 5f, transform.position.z), 0.8f, false);
    }
    public void PlayerRevive()
    {
        gameObject.transform.position = defaultPosition;   
        gravityForce = gravityForceLive;
    }
    private void FaceToFaceEnemy()
    {
        if(enemyLookTarget)
            transform.LookAt(enemyLookTarget.transform.position);
        float rotY = transform.eulerAngles.y;
        gravityForce = 0;
        transform.eulerAngles = new Vector3 (0, rotY, 0);
    }
   
    void GroundCheck()
    {
        RaycastHit hit;
        float distance = 1f;
        Vector3 dir = new Vector3(0, -1);

        if(Physics.Raycast(transform.position, dir, out hit, distance))
            isGround = true;
        else
        {
            GetComponent<Rigidbody>().velocity += Vector3.down * gravityForce;
            isGround = false;
        }
    }
    public void Stop()
    {
        foreach (var script in GetComponents<MonoBehaviour>())
        {
            script.enabled = false;
        }
    }
    public void Play()
    {
        foreach (var script in GetComponents<MonoBehaviour>())
        {
            script.enabled = true;
        }
    }

    public void ToggleRun(bool isRun)
    {
        speed = isRun ? runSpeed : walkSpeed;
    }
}
