using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ItemID
{
    None = 0,
    Object_1,
    Object_2,
    Object_3,
    Object_4,
    Object_5,
    Object_6,
    Object_7,
    Object_8,
    Object_9,
    Object_10,
    Object_11,
    Object_13,
    Object_14
}

public class PlayerInventory : PlayerScript
{
    private List<PuzzleItem> items;
    public GameObject arrow;
    public GameObject fuseContainer;

    protected override void Awake()
    {
        base.Awake();

        items = new List<PuzzleItem>();
    }

    public bool Contain(PuzzleItem item)
    {
        return items.Contains(item);
    }

    public void AddItem(PuzzleItem item)
    {
        if (items.Contains(item))
            return;

        items.Add(item);
        UI_Inventory.Instance.AddInventoryItemOnUI(item.ID);

        item.OnAddToInventory();
        if (arrow)
            ArrowDirectionOnFuse(item);
        UI_FuseCheck.Instance?.ShowPlayerPickedFusesUI(item);
    }

    public bool UseItem(PuzzleItem item)
    {
        if(Contain(item))
        {
            if (!item.IsReusable)
            {
                items.Remove(item);
                UI_Inventory.Instance.RemoveInventoryItemOnUI(item.ID);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ArrowDirectionOnFuse(PuzzleItem item)
    {
        List<GameObject> arrayOjbDirections = gameObject.GetComponentInChildren<ArrowDirection>().ojbDirections;
        for(int i = 0; i < arrayOjbDirections.Count - 1 ; i++)
        {
            if(item == arrayOjbDirections[i].GetComponent<PuzzleItem>())
            {
                gameObject.GetComponentInChildren<ArrowDirection>().indexOjb = i;
                if(i == arrayOjbDirections.Count - 2)
                    gameObject.GetComponentInChildren<ArrowDirection>().indexOjb = 0;
                gameObject.GetComponentInChildren<ArrowDirection>().ojbDirections.RemoveAt(i);
                gameObject.GetComponentInChildren<ArrowDirection>().listImageObject.RemoveAt(i);
              
                if(fuseContainer) fuseContainer.GetComponent<FuseShowOrHide>().UpdateActiveFuse();
            }
        }       
    }
    public void ShowStatusDirection()
    {
        string txtOnComplete = gameObject.GetComponentInChildren<ArrowDirection>().textOnCompleted;
        string OnLastCompleted = gameObject.GetComponentInChildren<ArrowDirection>().textOnLastCompleted;
        List<GameObject> arrayOjbDirections = gameObject.GetComponentInChildren<ArrowDirection>().ojbDirections;
        if(arrayOjbDirections.Count == 1)
            gameObject.GetComponentInChildren<ArrowDirection>().ShowStatusDirectionWithText(OnLastCompleted);  
        else
            gameObject.GetComponentInChildren<ArrowDirection>().ShowStatusDirectionWithText(txtOnComplete);
    }
}