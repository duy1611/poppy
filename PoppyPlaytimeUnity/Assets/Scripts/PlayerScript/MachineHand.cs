using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MachineHand : MonoBehaviour
{
    [SerializeField] private Transform hand;
    [SerializeField] private Transform arm;
    [SerializeField] private float handSpeed;
    [SerializeField] private float handReturnSpeed;

    internal Transform Hand { get { return hand; } }

    private Vector3 handOriginalPos;

    private Vector3 handStartPos;
    private Vector3 handEndPos;
    private bool isHandMoving;
    private float currentMoveTime;
    private float totalMoveTime;
    private bool handReturn;
    UnityAction onHandFinishMoving;

    private void Awake()
    {
        handOriginalPos = hand.localPosition;

    }

    private void Update()
    {
        UpdateHandPosition();
        MoveHand();
        ModifyArm();
    }
    

    private void ModifyArm()
    {
        arm.LookAt(hand.position, transform.up);
        hand.rotation = arm.rotation;
        arm.localScale = new Vector3(1f, 1f, (hand.position - arm.position).magnitude);
    }

    private void MoveHand()
    {
        if (!isHandMoving)
            return;

        currentMoveTime += Time.deltaTime;
        if(currentMoveTime >= totalMoveTime)
        {
            currentMoveTime = totalMoveTime;
            isHandMoving = false;
            onHandFinishMoving?.Invoke();
        }
        hand.position = Vector3.Lerp(handStartPos, handEndPos, currentMoveTime / totalMoveTime);
    }

    public void FireHand(Vector3 destination, bool isReturn = true, UnityAction onFinishFire = null, UnityAction onFinishPull = null)
    {
        if (isReturn)
        {
            SetupHandPath(destination, handSpeed, ()=>
            {
                onFinishFire?.Invoke();
                PullHand(onFinishPull);
                handReturn = true;
            });
        }
        else
        {
            SetupHandPath(destination, handSpeed, onFinishFire);
        }
    }

    public void PullHand(UnityAction onFinish = null)
    {
        SetupHandPath(handEndPos, handReturnSpeed, onFinish);
    }

    private void SetupHandPath(Vector3 destination, float speed, UnityAction onFinish = null)
    {
        handStartPos = hand.position;        
        handEndPos = destination;
        isHandMoving = true;
        handReturn = false;
        currentMoveTime = 0f;
        totalMoveTime = (handStartPos - handEndPos).magnitude / speed;
        onHandFinishMoving = onFinish;
    }
    private void UpdateHandPosition()
    {
        if(handReturn)
            handEndPos = transform.TransformPoint(handOriginalPos);
    }
}
