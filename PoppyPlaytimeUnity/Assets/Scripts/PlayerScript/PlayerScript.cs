using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private static Dictionary<System.Type, Component> components;

    protected virtual void Awake()
    {
        InitComponents();
    }

    private void InitComponents()
    {
        if (components == null)
        {
            components = new Dictionary<System.Type, Component>();

            foreach (var item in GetComponents<Component>())
            {
                var itemType = item.GetType();
                components.Add(itemType, item);
            }
        }
    }

    public static T Get<T>() where T : Component
    {
        if (components == null)
            return default;

        var type = typeof(T);
        if (components.ContainsKey(type))
        {
            return (T)components[type];
        }
        else
        {
            Debug.LogError("Component not found: " + type);
            return default;
        }
    }

    private void OnDestroy()
    {
        components = null;
    }
}
